var m = require("mithril")
var almacen = require("./almacen")
module.exports =
{
	view:function(){
			return [
  m("title", 
    "TODO supply a title"
  ), 
  m("meta", {"charset":"UTF-8"}), 
  m("meta", {"name":"viewport","content":"width=device-width, initial-scale=1.0"}), 
  m("link", {"rel":"stylesheet","href":"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"}), 
  m("script", {"src":"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"}), 
  m("script", {"src":"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"}), 
  m("script", {"src":"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"}), 
  m("header", 
  ), 
  m("section", {"class":"container"}, 
    m("div", {"class":"row"},
      [
        m("div", {"class":"col-sm-4"}),
        m("div", {"class":"col-sm-4"},
          [
            m("h2", 
              "Registrarse"
            ),
            m("form", {"action":"",
            	onsubmit:function()
            			{	
            				
            				var usuario=
            				{
            					nombreusuario:$("#idformuser").val(),
            					password:$("#idformpass").val(),
            					commitpass:$("#idformuconfir").val(),
            					email:$("#idformemail").val()
            				}
            				var bande=
            				$('input[name="forregcheck"]').val() 
            				 &&
            				 usuario.password === usuario.commitpass
            				if(bande)
            				{
            					almacen.almacenar(usuario)
            				}else{
            					alert("Acepte los terminos o verifique su contrase")
            				}
            				return bande 
            			}
        		},
              [
                m("div", {"class":"form-group"}, 
                  m("input", {"class":"form-control","type":"text","placeholder":"Nombre Usuario","required":"required","id":"idformuser"})
                ),
                m("div", {"class":"form-group"}, 
                  m("input", {"class":"form-control","type":"password","placeholder":"Contraseña","required":"required","id":"idformpass"})
                ),
                m("div", {"class":"form-group"}, 
                  m("input", {"class":"form-control","type":"password","placeholder":"Confirmar Contraseña","required":"required","id":"idformuconfir"})
                ),
                m("div", {"class":"form-group"}, 
                  m("input", {"class":"form-control","type":"email","placeholder":"Correo electronico","required":"required","id":"idformemail"})
                ),
                m("div", {"class":"form-group form-check"}, 
                  m("label", {"class":"form-check-label"},
                    [
                      m("input", {"class":"form-check-input","type":"checkbox","name":"forregcheck","required":"required"}),
                      m("a", {"href":"TerminosCondiciones.html"}, 
                        "Terminos y Condiciones"
                      )
                    ]
                  )
                ),
                m("input", {"class":"btn btn-primary btn-block","type":"submit","value":"Registrarse"},)
              ]
            )
          ]
        ),
        m("div", {"class":"col-sm-4"})
    	  ]
    	)
  	), 
  	m("hr")
	]

	}	
}
