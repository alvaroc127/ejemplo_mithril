var m = require("mithril")
var user = require("./User")

function valorGet(valor){
    var band=true
    console.log(JSON.stringify(valor))
    if(typeof valor === 'undefined'){
        band=false
        window.alert("ERROR con el usuario");
    }
    return band
}


module.exports ={
        oninit: user.loadList,
		view: function()
		{
        var usuarios=user.list
		return [
  m("title", 
    "Articulo APP"
  ), 
  m("meta", {"charset":"UTF-8"}), 
  m("meta", {"name":"viewport","content":"width=device-width, initial-scale=1.0"}), 
  m("link", {"rel":"stylesheet","href":"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"}), 
  m("script", {"src":"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"}), 
  m("script", {"src":"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"}), 
  m("script", {"src":"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"}), 
  m("header", 
  ), 
  m("section", {"class":"container"}, 
    m("div", {"class":"row"},
      [
        m("div", {"class":"col-sm-4"}),
        m("div", {"class":"col-sm-4"},
          [
            m("h2",
              [
                "Iniciar sesi",
                m.trust("&oacute;"),
                "n"
              ]
            ),
            m("form", {
                "action":"",
                onsubmit:function()
                    {   
                        var dumi=
                            {
                            username:$("#usuario").val(),
                            password:$("#password").val()
                            }
                    return valorGet(usuarios.find(userorg =>
                    userorg.firstName == dumi.username
                        &&
                    userorg.id == dumi.password))
                        }
                    },
              [
                m("div", {"class":"form-group"}, 
                  m("input", {"class":"form-control","type":"text","id":"usuario","placeholder":"Usuario","name":"usuario","required":"required"})
                ),
                m("div", {"class":"form-group"}, 
                  m("input", {"class":"form-control","type":"password","id":"password","placeholder":"Contraseña","name":"password","required":"required"})
                ),
                m("input", {"class":"btn btn-primary btn-block","type":"submit","value":"Iniciar sesi&oacute;n"}),
                m("br"),
                m("div", {"class":"form-group"}, 
                  m("p", {"class":"text-center"},
                    [
                      m("a", {"target":"_top","href":"Registro.html#idregistro"}, 
                        "Registrarse"
                      ),
                      m("a", {"target":"_top","href":"OlvideMiContrasenia.html"}, 
                        "Olvide Contraseña"
                      )
                    ]
                  )
                )
              ]
            )
          ]
        ),
        m("div", {"class":"col-sm-4","id":"useraut"})
      ]
    )
  ), 
  m("hr")
    ]
 }
}